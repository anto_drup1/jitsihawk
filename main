#!/bin/bash

# ----------------------------------------------------------------------
# MIT LICENSE
#
# Copyright 2020 Joost Snellink
#
# Permission is hereby granted, free of charge, to any person obtaining a 
# copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the 
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
# IN THE SOFTWARE.
# ----------------------------------------------------------------------

# Description
# ----------------------------------------------------------------------
# Entry point to Jitsihawk and Logsnatch
# Calls Logsnatch and Jitsihawk every $TIMEDELAY
# Exits after pressing Q

# Usage
# ----------------------------------------------------------------------
# Call `main -s $YOURSERVERDOMAIN`
# Logsnatch will need to access /var/log/jitsi directory, so either run as sudo 
# (not recommended) or set up the proper user priviledges to access the jicofo logs.


# User Definitions
# -----------------------------------------------------------------------------------------------------------
TIMEDELAY=1;



# Main code 
# -----------------------------------------------------------------------------------------------------------
# Set current directory
DIR="$(cd "$(dirname "$0")" && pwd)"

# Check for flags
while getopts ":s:" opt; do
  case $opt in
    s)
      DOMAIN="$OPTARG" >&2
	  ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Check if the $DOMAIN variables is set else exit with error
[ -z ${DOMAIN+x} ] && echo "Jitsihawk error:" && echo "Server domain not defined." && echo "Check if -s flag is set!" && echo "Exiting." && exit 1

# repeating loop with progress spinner
while true; do
	# Call logsnatch and wait a second
	$DIR/lib/logsnatch; sleep $TIMEDELAY &

	# Spinner code
	PID=$!
	i=1
	sp="/-\|"
	echo -n ' '
	while [ -d /proc/$PID ]
	do
		printf "\b${sp:i++%${#sp}:1}"
		# Delay added to reduce cpu load
		sleep 0.1
	done

	# Execute jitsihawk script with server domain as cli option
	$DIR/lib/jitsihawk $DOMAIN
	sleep $TIMEDELAY

	# Some code to break out of the loop if q/Q key is pressed
    read -t 0.25 -N 1 input
    if [[ $input = "q" ]] || [[ $input = "Q" ]]; then
        echo
        break 
    fi
done
